import { funcionesScript } from "./scripts.js";
const { eliminarSpinner, cargarSpinner } = funcionesScript;

const getMascotasPromise = (url) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", () => {
            if (xhr.readyState == 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    const data = JSON.parse(xhr.responseText);
                    resolve(data);
                } else {
                    reject({ status: xhr.status, statusText: xhr.statusText });
                }
            }
        });
        xhr.open("GET", url);
        xhr.send();
    });
};

export { getMascotasPromise };
