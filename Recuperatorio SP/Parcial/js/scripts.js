function cargarSpinner() {
    const tableContainer = document.getElementById("cards-container");

    const divSpinner = document.createElement("div");
    divSpinner.setAttribute("id", "div-spinner");
    divSpinner.setAttribute("class", "centrar");

    const image = document.createElement("img");
    image.setAttribute("src", "./assets/spinner.gif");
    image.setAttribute("alt", "Spinner de carga");
    image.setAttribute("id", "spinner-carga");
    divSpinner.appendChild(image);

    tableContainer.insertAdjacentElement("afterend", divSpinner);
}

function eliminarSpinner() {
    const divSpinner = document.getElementById("div-spinner");
    divSpinner.remove();
}

export const funcionesScript = {
    cargarSpinner,
    eliminarSpinner,
};
