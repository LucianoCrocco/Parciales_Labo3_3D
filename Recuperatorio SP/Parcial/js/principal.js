import crearCarta from "./card.js";
import { getMascotasPromise } from "./ajax.js";

const listaMascota = [];

const url = "http://localhost:3010/mascotas";
await getMascotasPromise(url)
    .then((items) => listaMascota.push(...items))
    .catch((e) => console.log(e));

const cardsContainer = document.querySelector("#cards-container");

listaMascota.forEach((element) => {
    const card = crearCarta(element);
    cardsContainer.appendChild(card);
});
