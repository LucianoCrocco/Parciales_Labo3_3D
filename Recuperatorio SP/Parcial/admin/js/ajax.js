import { funcionesScript } from "./scripts.js";
const { eliminarSpinner, cargarSpinner } = funcionesScript;

const getMascotasPromise = (url) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", () => {
            if (xhr.readyState == 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    const data = JSON.parse(xhr.responseText);
                    resolve(data);
                } else {
                    reject({ status: xhr.status, statusText: xhr.statusText });
                }
            }
        });
        xhr.open("GET", url);
        xhr.send();
    });
};

const postMascotaPromise = (url, mascota) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", () => {
            if (xhr.readyState == 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    const data = JSON.parse(xhr.responseText);
                    resolve(data);
                } else {
                    reject({ status: xhr.status, statusText: xhr.statusText });
                }
            }
        });
        xhr.open("POST", url);
        xhr.setRequestHeader("Content-Type", "application/json;charset=utf8");
        xhr.send(JSON.stringify(mascota));
    });
};

// ! EXPORTAR -> UTILIZAR EN APP.JS
const getMascotaAjax = (url, id) => {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", () => {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                const data = JSON.parse(xhr.responseText);
                console.log(data);
            } else {
                console.error(xhr.status, xhr.statusText);
            }
            eliminarSpinner();
        } else {
            cargarSpinner();
        }
    });
    xhr.open("GET", `${url}?id=${id}`);
    xhr.send();
};
const getMascotasAjaxAsync = async (url) => {
    try {
        cargarSpinner();
        const data = await getMascotasPromise(url);
        return data;
    } catch (err) {
        console.log(err);
    } finally {
        eliminarSpinner();
    }
};

const postMascotaAjaxAsync = async (url, mascota) => {
    try {
        cargarSpinner();
        const data = await postMascotaPromise(url, mascota);
        console.log(data);
    } catch (err) {
        throw err;
    } finally {
        eliminarSpinner();
    }
};

const putMascotaAjaxPromise = (url, mascota) => {
    cargarSpinner();
    return new Promise((resolve, reject) => {
        const options = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(mascota),
        };
        fetch(`${url}/${mascota.id}`, options)
            .then((res) => {
                return res.ok ? res.json() : reject(res);
            })
            .then((res) => resolve(res))
            .catch((err) => {
                throw err;
            })
            .finally(() => eliminarSpinner());
    });
};
const deleteMascotaAjaxPromise = (url, id) => {
    cargarSpinner();
    return new Promise((resolve, reject) => {
        const options = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        };
        fetch(`${url}/${id}`, options)
            .then((res) => {
                return res.ok ? res.json() : reject(res);
            })
            .then((res) => resolve(res))
            .catch((err) => {
                throw err;
            })
            .finally(() => eliminarSpinner());
    });
};
export {
    getMascotaAjax,
    getMascotasAjaxAsync,
    postMascotaAjaxAsync,
    putMascotaAjaxPromise,
    deleteMascotaAjaxPromise,
};
