export const validarCampoVacio = (e) => {
    const input = e.target;
    if (!input.classList.contains("inputError"))
        !input.value.trim()
            ? setError(input, "Campo requerido")
            : clearError(input);
};

export const validarPrecio = (e) => {
    const input = e.target;
    const precio = input.value.trim();
    console.log(precio);
    !(precio > 5000000 || precio < 1)
        ? clearError(input)
        : setError(input, "Precio invalido");
};

export const validarTexto = (e) => {
    const input = e.target;
    validarLongitudMaxima(input, 60)
        ? clearError(input)
        : setError(input, "Debe tener un maximo de 60 caracteres");
};

export const validarTodosLosCampos = (controles) => {
    for (const control of controles) {
        if (control.classList.contains("inputError") || !control.value)
            if (
                control.matches("[type=text]") ||
                control.matches("[type=number]") ||
                control.matches("[type=date]") ||
                control.matches("[type=select-one]")
            ) {
                return false;
            }
    }
    return true;
};

const validarLongitudMaxima = (input, maxima) =>
    input.value.trim().length <= maxima;

const setError = (input, mensaje) => {
    if (input.nextElementSibling.tagName != "SMALL") {
        const $small = document.createElement("small");
        input.insertAdjacentElement("afterend", $small);
        const name = input.name;
        $small.textContent = mensaje || `${name.toUpperCase()} campo Requerido`;
        $small.classList.add("danger");
        input.classList.add("inputError");
    }
};

export const clearError = (input, mensaje) => {
    const $small = input.nextElementSibling;
    if ($small.tagName == "SMALL") {
        $small.remove();
        input.classList.remove("inputError");
    }
};

const checkErrorSmall = (input) => {};
