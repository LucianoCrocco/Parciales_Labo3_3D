export default class Anuncio {
    constructor(id, titulo, descripcion, animal, precio) {
        try {
            this.id = id;
            this.titulo = titulo;
            this.descripcion = descripcion;
            this.animal = animal;
            this.precio = parseFloat(precio).toFixed(2);
        } catch (error) {
            throw error;
        }
    }
}
