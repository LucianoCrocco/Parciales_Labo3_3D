import Anuncio from "./Anuncio.js";
export default class Anuncio_Animal extends Anuncio {
    constructor(
        id,
        titulo,
        descripcion,
        animal,
        precio,
        raza,
        nacimiento,
        vacuna
    ) {
        try {
            super(id, titulo, descripcion, animal, precio);
            this.raza = raza;
            this.nacimiento = nacimiento;
            this.vacuna = vacuna;
        } catch (error) {
            throw error;
        }
    }
}
