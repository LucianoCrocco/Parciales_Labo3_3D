export default function crearTabla(cabecera, vec) {
    const table = document.createElement("table");
    table.setAttribute("class", "table-dark w-100 col-12");
    table.appendChild(crearCabecera(cabecera));
    table.appendChild(crearCuerpo(cabecera, vec));
    return table;
}

function crearCabecera(cabecera) {
    const $thead = document.createElement("thead");
    const $tr = document.createElement("tr");
    $thead.appendChild($tr);
    $tr.setAttribute("class", "col");

    cabecera.forEach((e) => {
        if (e != undefined) {
            const $th = document.createElement("th");
            $th.textContent = e.toUpperCase();
            $tr.appendChild($th);
        }
    });

    return $thead;
}

function crearCuerpo(cabecera, vec) {
    const $tbody = document.createElement("tbody");
    vec.forEach((element, index) => {
        const $tr = document.createElement("tr");
        $tbody.appendChild($tr);
        Object.keys(element).forEach((key) => {
            if (cabecera.includes(key)) {
                const $td = document.createElement("td");
                if (key === "id") {
                    $tr.dataset.id = index;
                } else {
                    $td.textContent = element[key];
                    $tr.appendChild($td);
                }
            }
        });
    });
    return $tbody;
}
