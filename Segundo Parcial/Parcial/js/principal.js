import crearCarta from "./card.js";

const listaMascota = localStorage.getItem("mascotas")
    ? JSON.parse(localStorage.getItem("mascotas"))
    : [];
const cardsContainer = document.querySelector("#cards-container");

listaMascota.forEach((element) => {
    const card = crearCarta(element);
    cardsContainer.appendChild(card);
});
